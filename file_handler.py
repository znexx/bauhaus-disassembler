import re

def read(filename):
	try:
		raw_bytes = open(filename, "rb").read()
	except IOError:
		raise IOError('Could not open file for reading')
	return raw_bytes

def write(filename, binary):
	try:
		out_file = open(filename, 'wb')
		out_file.write(binary)
	except IOError:
		raise IOError('Could not open file for writing')
