#!/usr/bin/python3

import sys
import logging
import file_handler

import archs.bauhaus2 as arch

#TODO:
# - recognize instructions
# - we may need linking, with segments... ELF format?
# - shared mnemonics file with assembler?
# - output assembler

class config:
	absolute_adresses = True
	print_opcodes = False

def parse_instruction(i):
	opcode = binary_file[i]
	mnemonic = arch.instructions[opcode][0]
	instruction_size = arch.instructions[opcode][1] + 1
	return [opcode, mnemonic, instruction_size]

def find_instructions(binary_file):
	locations = []
	logging.debug("looking for instructions:")
	visit(locations, binary_file, 0)
	logging.debug("locations visited:")
	logging.debug(locations)
	return locations

def visit(l, b, i):
	if i >= len(b) or i in l:
		return
	[opcode, mnemonic, instruction_size] = parse_instruction(i)
	logging.debug("{:4d} {:2d} {:4} {:2d}".format(i, opcode, mnemonic, instruction_size))
	l.append(i)
	if opcode == 0x20:
		visit(l, b, i + sign_byte(b[i + 1]))
	elif opcode & 0xf0 == 0x20:
		visit(l, b, i + sign_byte(b[i + 3]))
		visit(l, b, i + instruction_size)
	else:
		visit(l, b, i + instruction_size)

def sign_byte(b):
	if b > 127:
		return b - 256
	else:
		return b

def format_instruction(opcodes, absolute_adresses, i):
	s = ""
	[opcode, mnemonic, instruction_size] = parse_instruction(i)
	if opcodes:
		s = s + '{:2x} '.format(opcode)

	s = s + '{:4} '.format(mnemonic)
	for n in range(1, instruction_size):
		value = sign_byte(binary_file[i + n])
		if absolute_adresses:
			value = i + value
		s = s + '{:4d}'.format(value)
		if n != instruction_size - 1:
			s = s + ', '
	return [s, instruction_size]

def print_asm(filename, binary_file, locations_visited):
	out_file = open(filename, 'w')

	last_was_instruction = True
	i = 0
	while i < len(binary_file):
		if i in locations_visited:
			if not last_was_instruction:
				out_file.write(data + "\n")
			[instruction, instruction_size] = format_instruction(False, False, i)
			out_file.write(instruction + "\n")
			last_was_instruction = True
			i = i + instruction_size
		else:
			if last_was_instruction:
				data = 'dw '
			else:
				data = data + ", "
			data = data + '{:4d}'.format(binary_file[i])
			last_was_instruction = False
			i = i + 1
	out_file.close()

def print_full(binary_file, locations_visited):
	i = 0
	while i < len(binary_file):
		print('{:4d}  '.format(i), end="")
		if i in locations_visited:
			[instruction, instruction_size] = format_instruction(True, True, i)
			print(instruction)
			i = i + instruction_size
		else:
			print('{:2x}  '.format(binary_file[i]))
			i = i + 1

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)

	in_file = ""
	out_file = ""
	arguments = sys.argv
	arguments.reverse()
	arguments.pop()
	while arguments:
		arg = arguments.pop()
		if arg == "-v":
			logging.getLogger().setLevel(logging.DEBUG)
		elif arg == "-q":
			logging.getLogger().setLevel(logging.ERROR)
		elif arg == "-o":
			try:
				out_file = arguments.pop()
			except IndexError as err:
				logging.critical("No filename given to -o")
				sys.exit(1)
		else:
			in_file = arg

	try:
		in_file = sys.argv[1]
	except IndexError as err:
		logging.debug("No filename specified".format())

	logging.debug("Reading file {0}".format(in_file))
	try:
		binary_file = file_handler.read(in_file)
	except IOError as err:
		logging.debug("File reading error in {}: {}".format(filename, err))

	try:
		locations_visited = find_instructions(binary_file)
	except IOError as err:
		logging.debug("File reading error in {}: {}".format(filename, err))

	print_full(binary_file, locations_visited)
	if out_file:
		print_asm(out_file, binary_file, locations_visited)
